
def ord_list(src):
    """convierte a integer los elementos de un list de caracteres"""
    return [ ord(c) for c in list(src) ]

def chr_list(src):
    """convierte a caracteres los elementos de un list de integers"""
    return [ chr(i) for i in list(src) ]

def rsort_lsd(a, base = 10):
    """Ordena enteros usando el digito menos significativo"""
    nd = max([ len(str(i)) for i in list(a) ])

    for d in range(nd):
        bins = [[] for i in range(base)]

        for v in a:
            k = (v // 10 ** d) % base
            bins[k].append(v)

        a = []

        for section in bins:
            a.extend(section)

    return a
# rsort



#function bucketSort(array, n) is
#  buckets ← new array of n empty lists
#  for i = 0 to (length(array)-1) do
#    insert array[i] into buckets[msbits(array[i], k)]
#  for i = 0 to n - 1 do
#    nextSort(buckets[i])
#  return the concatenation of buckets[0], ..., buckets[n-1]


if __name__ == '__main__':
    
    digit = [1021, 14356, 5070, 12447, 139, 1704, 1363, 14583, 4392, 1196, 13609, 10000, 128, 1117, 873, 12778]
    
    word = ['aba' , 'ana', 'sometimes', 'gold', 'it', 'is', 'useful', 'to', 'be', 'able', 'to', 'reproduce', 'the', 'sequences', 'given', 'by', 'a', 'pseudo', 'random', 'number', 'generator']

    













