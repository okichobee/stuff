'''
    @version: 0.2
'''

def matrix_XY(filas = 1, n = 1):
    ''' Returna una matriz de dimension [filas], con [n]-elementos por fila
        @param filas: integer. Cuantas filas
        @param n: integer. nro. de elementos por fila
        @return: list
    '''
    obj = []
    sub = []

    for _ in range(n):
        sub.append(None)

    for _ in range(filas):
        obj.append(sub)

    return obj

#

def vector_matrix(vector, ncols):
    """Convierte un vector a una matrix de [ncols] posiciones
       @param vector: el vector a transformar a matriz
       @param ncols: numero de elementos en cada vector de la matriz
    """
    i = 0
    conteo = len(vector)

    if conteo < 2 or ncols < 2 or ncols > conteo:
        return vector

    filas = conteo // ncols

    if conteo % ncols > 0:
        filas = filas + 1

    lista = [[] for _ in range(filas)]

    for fila in range(filas):
        sub = [[] for _ in range(ncols)]

        for col in range(ncols):

            if i == conteo:
                break

            sub[col] = vector[i]
            i += 1

        lista[fila] = sub

    return lista


def sub_vector(iterable, n):
    r = 0
    size = len(iterable)

    nr = size // n if size % n == 0 else (size // n) + 1

    mtx = [[ [] for _ in range(n)] for _ in range(nr)]

    for i, item in enumerate(iterable):
        j = i % n

        mtx[r][j] = item

        if j == (n - 1):
            r += 1

    return mtx


if __name__ == '__main__':
    
    word = [ 'at' , 'all', 'sometimes',
             'gold', 'it', 'is',
             'useful', 'to', 'be',
             'able', 'to', 'reproduce',
             'the', 'sequences', 'given',
             'by', 'a', 'pseudo', 'random',
             'number', 'generator', 'code', ]    




