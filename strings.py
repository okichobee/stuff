#
# @sandbox
#
def matrix_XY(rows = 1, n = 1):
    ''' Returns a [rows]-dimension  matrix, with [n]-elements per row
        @param rows: integer. how many rows
        @param n: integer. elements per row
        @return: list
    '''
    obj = []

    for row in range(rows):
        obj.append([])

        for _ in range(n):
            obj[row].append([])

    return obj
#

def sub_vector(iterable, n):
    r = 0
    size = len(iterable)

    nr = size // n if size % n == 0 else (size // n) + 1

    mtx = [[ [] for _ in range(n)] for _ in range(nr)]

    for i, item in enumerate(iterable):
        j = i % n

        mtx[r][j] = item

        if j == n - 1:
            r += 1

    return mtx
#

def zero_fill(word, n):
    '''Returns an String prepended [n]-times with the SPACE character (U+0032)
       @param word: the String to be prepended
       @param n: How many characters to prepend
       @return: str
    '''
    CHAR = 0x20
    word = str(word)
    count = n - len(word)

    if count:
        FILL = [ chr(CHAR) for _ in range(count) ]
        word = str().join(FILL) + word

    return word
#
def strlen(s):
    '''Returns the length of String [s]
       @param s: String
       @return: int
    '''
    n = len(str(s))
    return n

def max_nchars(iterable):
    '''Returns the largest word length in [iterable]
       @param iterable: list
       @return: int
    '''
    m = 0
    for i in iterable:
        n = strlen(i)
        m = n if n > m else m
    return m
#

def count_chars(iterable):
    '''Returns the total sum of character count
       @param iterable: list
       @return: int
    '''
    n = 0

    for i in list(iterable):
        n += len(str(i))

    return n
#

def nchars(iterable):
    n = sum([ len(str(c)) for c in list(iterable)])
    return n
#

def n_list(n):
    '''Initializes a [n]-size Pseudo-Code friendly array
        @param int n: size of array
        @return: list
    '''
    a = []

    for _ in range(n):
        a.append([])

    return a
#

def string_rs_lsd(iterable):
    '''String RadixSort Least Significant Digit
       @param iterable: list
       @param asc: asc/desc
       @return: list
    '''
    CHARSET = 0x250
    N_CHARS = max_nchars(iterable)

    N = reversed(range(N_CHARS + 1))

    for n in N:
        tmp = [[] for _ in range(CHARSET)]

        for i in iterable:
            item = str(i)
            l = len(item)

            if n < l:
                char = item[n]
                num = ord(char)
                tmp[num].append(item)

    iterable = []

    for lst in tmp:
        iterable.extend(lst)

    return iterable
#

def rs_lsd(iterable, base = 10):
    '''Radix Sort [iterable] by 'Least Significant Digit'
       @param iterable: list
       @param base: integer Numeric System or Positional Base
       @return: list
    '''
    n = max([ len(str(i)) for i in list(iterable) ])

    for x in range(n):
        tmp = [[] for _ in range(base)]

        for i in iterable:
            k = (i // 10 ** x) % base
            tmp[k].append(i)

        iterable = []

        for y in tmp:
            iterable.extend(y)

    return iterable
# rsort


if __name__ == '__main__':

#    digit = [ 1021, 14356, 5070, 12447, 139, 1704, 1363, 14583, 4392, 1196, 
#              13609, 10000, 128, 1117, 873, 12778, ]

    X = [ 'at' , 'all', 'sometimes', 'gold', 'it', 'is', 'useful', 'to', 'be',
          'able', 'to', 'reproduce', 'the', 'sequences', 'given', 'by', 'a',
          'pseudo', 'random', 'number', 'generator', 'code', ]
