'''
@version: 0.7
'''
def anchoPalabra(string):
    '''Retorna cantidad de caracteres contenidos en [string]
       @param string: String
       @return: integer
    '''
    total = len(str(string))
    return total

def maxCaracteres(iterable):
    '''Retorna longitud de caracteres en la palabra mas larga contenida en [iterable]
       @param iterable: list
       @return: integer
       '''
    total = 0
    for elem in iterable:
        conteo = anchoPalabra(elem)
        total = conteo if conteo > total else total
    return total
#

def ordenarAnchoPalabraAscendente(iterable):
    '''Ordena [iterable] segun cantidad de caracteres (de menor a mayor)
       @param iterable: list
       @return: list
    '''
    base = maxCaracteres(iterable)
    tmp = [[] for _ in range(base + 1)]
    lista = []

    for elem in iterable:
        conteo = anchoPalabra(elem)
        tmp[conteo].append(elem)

    for grupo in tmp:
        lista.extend(grupo)

    return lista
#

def ordenarAnchoPalabraDescendente(iterable):
    '''Ordena [iterable] segun cantidad de caracteres (de mayor a menor )
       @param iterable: list
       @return: list
    '''
    base = maxCaracteres(iterable)
    tmp = [[] for _ in range(base + 1)]
    lista = []

    for elem in iterable:
        conteo = anchoPalabra(elem)
        tmp[conteo].append(elem)

    for i in range(len(tmp), 0, -1):
        lista.extend(tmp[i - 1])

#    for grupo in reversed( tmp ):
#        lista.extend( grupo )

    return lista
#

def radix_sort_lsd(iterable, base = 10):
    '''Ordena [iterable] usando el metodo 'digito menos significativo'
       @param iterable: list
       @param base: integer sistema numerico o base posicional
       @return: list
    '''
    maximo = max([ len(str(i)) for i in list(iterable) ])

    for x in range(maximo):
        tmp = [[] for _ in range(base)]

        for val in iterable:
            i = (val // 10 ** x) % base

            tmp[i].append(val)

        iterable = []

        for grupo in tmp:
            iterable.extend(grupo)

    return iterable
# rsort

def str_rs_lsd(iterable , asc = True):
    '''Ordena [iterable] usando RadixSort tipo Least Significant Digit
       @param iterable: list
       @param asc: Direccion de ordenamiento (ascendente/descendente)
    '''
    CHARSET = 0x250
    base = maxCaracteres(iterable)
    pases = reversed(range(base + 1))

    for pase in pases:
        tmp = [[] for _ in range(CHARSET)]

        for elem in iterable:
            item = str(elem)
            longitud = len(item)

            if pase < longitud:
                char = item[pase]
                num = ord(char)
                tmp[num].append(item)

                if asc:
                    tmp[num] = ordenarAnchoPalabraAscendente(tmp[num])
                else:
                    tmp[num] = ordenarAnchoPalabraDescendente(tmp[num])
    iterable = []

    for grupo in tmp:
        iterable.extend(grupo)

    return iterable
#
