'''
'''

def int2bin(n):
    """
    returns the binary representation of integer [n]
    @param n: the integer to convert
    @return: list
    """ 
    n = int(n)
    l = []
    
    while( n > 0 ):
        l.insert(0, n%2)
        n = n//2
        
    return l
    

if __name__ == '__main__':

    fn = 1234.5678;
    
    i,f = divmod(fn, 1);
     
    f = str(f)[2:]; 
    
    for n in f:
        print(int2bin(n))
    
    