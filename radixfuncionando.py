import time
from math import log
#pasa a numeros los caracteres y viceversa 
def chtoint (lista):
        n=int( len(lista))
        new_list = makeBlanks(n)
        for i in range (n):
            new_list[i] = ord(lista[i])
        return new_list

def intcha (lista):
        n=int(len(lista))
        new_list = makeBlanks(n)
        for i in range (n):
            new_list[i] = chr(lista[i])
        return new_list
    
###############################################
 
def getDigit(num, base, digit_num):
    # Saca el digito seleccionado
    return (num // base ** digit_num) % base  
 
def makeBlanks(size):
    # crear una lista de listas vacías para mantener la división en un dígito
    return [ [] for _ in range(size) ]  
 
def split(a_list, base, digit_num):
    buckets = makeBlanks(base)
    for num in a_list:
        # añadir el número a la lista seleccionada por el dígito
        buckets[getDigit(num, base, digit_num)].append(num)  
    return buckets
 
# concatenar las listas en orden para el siguiente paso
def merge(a_list): 
    new_list = []
    for sublist in a_list:
        new_list.extend(sublist)
    
    return new_list
 
def maxAbs(a_list):
    # abs mayor elemento de valor de una lista
    return max(abs(num) for num in a_list)  
 
def radixSort(a_list, base):
    #hay pases, tantas como son dígitos en el número más largo
    passes = int(log(maxAbs(a_list), base) + 1) 
    new_list = list(a_list)
    for digit_num in range(passes):
        new_list = merge(split(new_list, base, digit_num))
    return new_list


if __name__ == '__main__':
      
    opc=0
    
    while opc!=3:
        print("Ordenamiento Radix Sort")
        print("1. Ordenar cadena numerica ")
        print("2. Ordenar cadena alfanumerica ")
        print("4. Salir")
        opc=int(input("\nSeleccione una opcion: "))

        if opc<3:
            if (opc==1):
                lista = [100, 2, 99, 7, 75, 3, 8, 9, 84, 4]
                base =3
                t0 = time.clock()
                radix = radixSort(lista, base)
                tf = time.clock() - t0
                print("Lista original",lista)
                print("Lista ordenada",radix)
                print ("tiempo de ejecucion %f"%tf)
            if (opc==2):
                lista = ['f','h','a']
                num = chtoint(lista)
                base =3
                t0 = time.clock()
                radix = radixSort(num, base)
                tf = time.clock() - t0
                radixc = intcha(radix)
                print("Lista original",lista)
                print("Lista ordenada",radixc)
                print ("tiempo de ejecucion %f"%tf)

