
from radixfuncionando import radixSort

def listablanco( size ):
    return [ [] for i in range( size ) ]

def pluschar( lista ):
    c = int( len( lista ) )
    char = listablanco( c )
    if ( c > 1 ):
        for i in range ( c ):
            char[i] = ord( lista[i] )
        radix = radixSort( char, 3 )
        return intcha( radix )

def chtoint ( lista ):
    n = len( lista )
    new_list = listablanco( n )
    for i in range ( n ):
        val = len( lista[i] )
        if ( val > 1 ):
            new_list[i] = str().join( pluschar ( lista[i] ) )
        else:
            new_list[i] = ord( lista[i] )
            
    return new_list

def intcha ( lista ):
    n = len( lista )
    new_list = listablanco( n )

    for i in range ( n ):
        if isinstance( lista[i], int ):
            new_list[i] = chr( lista[i] )
        else:
            new_list[i] = lista[i]
                
    return new_list

if __name__ == '__main__':
    #lista = ['f','b','a','t','p']
    lista = ['fa', 'bz', 'c', 'a', 'f', 'b', 'a', 't', 'p']
    num = chtoint( lista )
    mm = intcha(num)
    print ( mm )




